(function ($) {
  Drupal.behaviors.barrut = {
    attach: function (context) {
      menu_user_behavior(context);
      adjuntos_behavior(context);

     Drupal.settings["panelsTabs"]= {};
     Drupal.settings.panelsTabs["tabsID"]= ["tabs-0-left"];
     
     var tabsID = Drupal.settings.panelsTabs.tabsID;

     for (var key in tabsID) {  
        var tabs = $('#' + tabsID[key], context);

        tabs.find('ul.pager li a').each(function() {
          a = $(this);
          parent_tab = a.parents('.ui-tabs-panel');
          a.attr('href', a.attr('href') + '#' + parent_tab.attr('id'));
        });
      } 

    }
  };

  function menu_user_behavior(context) {
    var menu_user = $('body.logged-in #block-system-user-menu ul', context);
    menu_user.mouseenter(function(e) {
      $(this).css({'overflow':'visible','border':'1px solid #ccc','height':'auto','background':'#FFF'});
      $('body.logged-in #block-system-user-menu ul li.first a', context).css({'color':'#999'});
      $('body.logged-in #block-system-user-menu ul li.first', context).addClass('menu_user_white');
      return false;
    });
    menu_user.mouseleave(function(e) {
      $(this).css({'overflow':'hidden','border':'none','height':'24px','background':'none'});
      $('body.logged-in #block-system-user-menu ul li.first a', context).css({'color':'#C1001F'});
      $('body.logged-in #block-system-user-menu ul li.first', context).removeClass('menu_user_white');
      return false;
    });
  }
  function adjuntos_behavior(context) {
    var adjunto = $('#total_adj .views-field-fid .field-content', context).click(function() {
	 $(this).parent().parent().parent().parent().parent().parent().parent().children("#listado_adj").toggle();
         //$(this).css({'color':'#999'});
         $(this).toggleClass("class_adjuntos_gris");
      });
  }
})(jQuery);

;
