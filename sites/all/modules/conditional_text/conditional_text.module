<?php

/**
 * @file
 * Hook implementations.
 */

/**
 * Constant for the module name.
 */
define('CONDITIONAL_TEXT_MODULE', 'conditional_text');

/**
 * Condition plugin.
 */
define('CONDITIONAL_TEXT_CONDITION_PLUGIN', 'condition');

/**
 * Display plugin.
 */
define('CONDITIONAL_TEXT_DISPLAY_PLUGIN', 'display');

/**
 * Minimum version of the plugins.
 */
define('CONDITIONAL_TEXT_MINIMUM_VERSION', 1.0);

/**
 * Current plugin version.
 */
define('CONDITIONAL_TEXT_VERSION', 1.0);

/**
 * Defines a prefix arity for an operator.
 */
define('CONDITIONAL_TEXT_ARITY_PREFIX', 1);

/**
 * Defines an infix arity for an operator.
 */
define('CONDITIONAL_TEXT_ARITY_INFIX', 2);

/**
 * Implements hook_init().
 */
function conditional_text_init() {
  if (!empty($_POST)) {
    conditional_text_load_all_condition_form_includes();
  }
}

/**
 * Implements hook_filter_info().
 */
function conditional_text_filter_info() {
  $filters = array();

  $filters['conditional_text'] = array(
    'title' => t('Conditional text'),
    'process callback' => 'conditional_text_filter_process',
    'settings callback' => 'conditional_text_filter_settings',
    'tips callback' => 'conditional_text_filter_tips',
    'default settings' => array(
      'display' => 'filter',
    ),
    'cache' => FALSE,
  );

  return $filters;
}

/**
 * Implements hook_theme().
 */
function conditional_text_theme() {
  return array(
    'conditional_text_nodisplay' => array(
      'variables' => array(
        'text' => '',
        'reason' => '',
      ),
    ),
    'conditional_text_fieldset' => array(
      'variables' => array(
        'text' => '',
        'reason' => '',
        'result' => NULL,
      ),
      'template' => 'conditional_text_fieldset',
    ),
    'conditional_text_fieldset_open' => array(
      'variables' => array(
        'text' => '',
        'reason' => '',
      ),
    ),
    'conditional_text_fieldset_closed' => array(
      'variables' => array(
        'text' => '',
        'reason' => '',
      ),
    ),
    'conditional_text_form_table' => array(
      'render element' => 'element',
    ),
    'conditional_text_form_tr' => array(
      'render element' => 'element',
    ),
    'conditional_text_form_th' => array(
      'render element' => 'element',
    ),
    'conditional_text_form_td' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_type().
 */
function conditional_text_ctools_plugin_type() {
  return array(
    CONDITIONAL_TEXT_CONDITION_PLUGIN => array(),
    CONDITIONAL_TEXT_DISPLAY_PLUGIN => array(),
  );
}

/**
 * Returns all plugins.
 *
 * @return array
 *   Associative array, keyed by the type of the array.
 */
function conditional_text_get_all_plugins() {
  return drupal_map_assoc(array(
    CONDITIONAL_TEXT_CONDITION_PLUGIN,
    CONDITIONAL_TEXT_DISPLAY_PLUGIN,
  ), 'conditional_text_get_plugins');
}

/**
 * Returns a specified plugin.
 *
 * @param string $type
 *   Type of the plugin. Can be CONDITIONAL_TEXT_CONDITION_PLUGIN or
 *   CONDITIONAL_TEXT_DISPLAY_PLUGIN.
 * @param null|string $plugin
 *   Name of the plugin. If NULL is given, then all plugins will be returned
 *   for the given type.
 *
 * @return array
 *   Plugin definition or the plugin list.
 */
function conditional_text_get_plugin($type, $plugin = NULL) {
  ctools_include('plugins');
  return ctools_get_plugins(CONDITIONAL_TEXT_MODULE, $type, $plugin);
}

/**
 * Returns all plugins for a given type.
 *
 * @param string $type
 *   Type of the plugin. Can be CONDITIONAL_TEXT_CONDITION_PLUGIN or
 *   CONDITIONAL_TEXT_DISPLAY_PLUGIN.
 *
 * @return array
 *   List of the plugin definitions.
 */
function conditional_text_get_plugins($type) {
  return conditional_text_get_plugin($type);
}

/**
 * Loads all possible includes for the plugin settings form.
 */
function conditional_text_load_all_condition_form_includes() {
  foreach (conditional_text_get_plugins(CONDITIONAL_TEXT_CONDITION_PLUGIN) as
    $plugin) {
    if (!empty($plugin['settings include']) &&
        file_exists($plugin['settings include'])) {
      include_once($plugin['settings include']);
    }
  }
}

/**
 * Implements hook_ctools_plugin_api().
 */
function conditional_text_ctools_plugin_api($module, $api) {
  if ($module == CONDITIONAL_TEXT_MODULE) {
    switch ($api) {
      case CONDITIONAL_TEXT_CONDITION_PLUGIN:
        return array(
          'version' => 1.0,
        );
      case CONDITIONAL_TEXT_DISPLAY_PLUGIN:
        return array(
          'version' => 1.0,
        );
    }
  }

  return NULL;
}

/**
 * Includes a given API.
 *
 * @param string $plugin
 *   Plugin name.
 *
 * @return array
 *   API information.
 */
function conditional_text_include_api($plugin) {
  ctools_include('plugins');
  return ctools_plugin_api_include(
    CONDITIONAL_TEXT_MODULE,
    $plugin,
    CONDITIONAL_TEXT_MINIMUM_VERSION,
    CONDITIONAL_TEXT_VERSION
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function conditional_text_ctools_plugin_directory($module, $plugin) {
  if ($module == CONDITIONAL_TEXT_MODULE) {
    switch ($plugin) {
      case CONDITIONAL_TEXT_CONDITION_PLUGIN:
        return 'plugins/condition';
      case CONDITIONAL_TEXT_DISPLAY_PLUGIN:
        return 'plugins/display';
    }
  }

  return NULL;
}

/**
 * Returns an empty text.
 *
 * @return string
 */
function theme_conditional_text_nodisplay() {
  return '';
}

/**
 * Helper function for theme_conditional_text_form_*.
 *
 * @param array $variables
 *   $variables array of the theme function.
 * @param string $tag
 *   The tag, which surrounds the form element.
 *
 * @return string
 *   Form element, wrapped with $tag.
 */
function _theme_conditonal_text_form_element($variables, $tag) {
  return "<{$tag}>{$variables['element']['#children']}</{$tag}>";
}

/**
 * Wraps a form element around with the <table> tag.
 */
function theme_conditional_text_form_table($variables) {
  return _theme_conditonal_text_form_element($variables, 'table');
}

/**
 * Wraps a form element around with the <tr> tag.
 */
function theme_conditional_text_form_tr($variables) {
  return _theme_conditonal_text_form_element($variables, 'tr');
}

/**
 * Wraps a form element around with the <th> tag.
 */
function theme_conditional_text_form_th($variables) {
  return _theme_conditonal_text_form_element($variables, 'th');
}

/**
 * Wraps a form element around with the <td> tag.
 */
function theme_conditional_text_form_td($variables) {
  return _theme_conditonal_text_form_element($variables, 'td');
}

/**
 * Returns an open fieldset.
 */
function theme_conditional_text_fieldset_open($variables) {
  return theme('conditional_text_fieldset',
    array('result' => TRUE) + $variables);
}

/**
 * Returns a closed fieldset.
 */
function theme_conditional_text_fieldset_closed($variables) {
  return theme('conditional_text_fieldset',
    array('result' => FALSE) + $variables);
}

/**
 * Processing callback for 'conditional_text' filter.
 */
function conditional_text_filter_process($text, $filter) {
  module_load_include('inc', 'conditional_text');

  // Load js and css files. Since caching is disabled in this filter it is safe
  // to assume that once a text is going to be filtered it will be also
  // displayed.
  $plugin = conditional_text_get_plugin(CONDITIONAL_TEXT_DISPLAY_PLUGIN,
                                        $filter->settings['display']);
  if ($plugin) {
    array_map('drupal_add_js', $plugin['js']);
    array_map('drupal_add_css', $plugin['css']);
  }

  return conditional_text_parse($text, $filter->settings);
}

/**
 * Settings callback for 'conditional_text' filter.
 */
function conditional_text_filter_settings($form,
                                          &$form_state,
                                          $filter,
                                          $format,
                                          $defaults,
                                          $filters) {
  module_load_include('inc', 'conditional_text');

  $filter->settings += $defaults;

  $elements = array();

  $printable_name = function($item) {
    return "{$item['title']} ({$item['description']})";
  };

  $elements['display'] = array(
    '#type' => 'radios',
    '#title' => t('Display'),
    '#options' => array_map($printable_name,
        array_sort(conditional_text_get_plugins(CONDITIONAL_TEXT_DISPLAY_PLUGIN),
          function($a, $b) use($printable_name) {
            return strcmp($printable_name($a), $printable_name($b));
          })),
    '#default_value' => $filter->settings['display'],
  );

  $elements['#attached']['css'][] =
    drupal_get_path('module', 'conditional_text') .
      '/hack-for-1015798.css';

  $elements += conditional_text_build_condition_settings_form($form_state,
                                                              $filter);

  return $elements;
}

/**
 * Filter callback for 'conditional_text' filter.
 */
function conditional_text_filter_tips($filter, $format, $long) {
  module_load_include('inc', 'conditional_text');

  $opsByArity = function($arity) {
    return array_keys(array_filter(
      conditional_text_get_operators(), function($op) use($arity) {
        return $op['arity'] === $arity;
      }));
  };

  $tips = $long ?
    t('Indicate conditional text using this syntax: ' .
      '[condition TYPE DETAILS](conditional text here)[/condition], where:' .
      '<ul>' .
      '<li>TYPE is the name of the condition plugin, ' .
        'such as enabled, generic, etc. You can install plugins ' .
        'to give you additional types of conditions.</li>' .
      '<li>DETAILS depends on TYPE, and may not be required for all ' .
        'condition plugins.</li>' .
      '</ul>' .
      'The type plugins you have enabled are listed below.<br /><br />' .
      'You can also make compound conditions, such as: ' .
      '[condition TYPE DETAILS OP TYPE2 DETAILS2], ' .
      'where OP is one of the following: @oplist. ' .
      'It is also possible to use prefixes: ' .
      '[condition OP TYPE DETAILS], ' .
      'where OP is one of the following: @preflist. ' .
      'Prefixed and compound conditions can also be mixed.<br /><br />' .
      'Details of installed types:',
      array(
        '@oplist' => implode(', ', $opsByArity(2)),
        '@preflist' => implode(', ', $opsByArity(1)),
      )) :
    t('Indicate conditional text using this syntax: ' .
      '[condition TYPE DETAILS](conditional text here)[/condition]. <br />' .
      'Compound conditions: [condition TYPE DETAILS OP TYPE2 DETAILS], ' .
      'where OP is one of the following: @oplist. <br />' .
      'Prefixed conditions: [condition OP TYPE DETAILS], ' .
      'where OP is one of the following: @preflist. <br />' .
      'Prefixed and compound conditions can be mixed. <br />' .
      'Details of supported types:',
      array(
        '@oplist' => implode(', ', $opsByArity(2)),
        '@preflist' => implode(', ', $opsByArity(1)),
      ));

  $plugin_tips = conditional_text_collect_filter_tips($filter, $format, $long);
  sort($plugin_tips, SORT_STRING);
  $tips .= theme('item_list', array(
      'items' => $plugin_tips,
    ));

  return $tips;
}

/**
 * Collects the operator definitions.
 *
 * @return array
 *   List of operator definitions.
 */
function conditional_text_get_operators() {
  return invoke_info_hook('conditional_text_operators');
}

/**
 * Implements hook_conditional_text_operators().
 */
function conditional_text_conditional_text_operators() {
  return array(
    'not' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_PREFIX,
      'evaluate' => function($exp) { return !$exp; },
    ),
    'and' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return $exp0 && $exp1; },
    ),
    'or' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return $exp0 || $exp1; },
    ),
    'xor' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return ($exp0 xor $exp1); },
    ),
    'nor' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return !($exp0 || $exp1); },
    ),
    'nand' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return !($exp0 && $exp1); },
    ),
    'imp' => array(
      'arity' => CONDITIONAL_TEXT_ARITY_INFIX,
      'evaluate' => function($exp0, $exp1) { return $exp0 ? $exp1 : TRUE; },
    ),
  );
}
