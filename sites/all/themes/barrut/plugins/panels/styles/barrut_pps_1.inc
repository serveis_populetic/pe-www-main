<?php

$plugin = array(
	'title' => t('Barrut: H4 title under the content'),
	'render pane' => 'barrut_pps_1',
);

function theme_barrut_pps_1($variables) {
  $content = $variables['content'];
  $pane = $variables['pane'];

  if (empty($content->content)) {
    return;
  }

  $attributes = array(
    'class' => array(
    	'panel-pane',
    	'panel-'. ctools_cleanstring($content->type, array('lower case' => TRUE)),
    	'pane-'. ctools_cleanstring($content->subtype, array('lower case' => TRUE)),
    ),
  );
  if (isset($pane->css['css_id'])) {
  	$attributes['id'] = $pane->css['css_id'];
  }
  if (isset($pane->css['css_class'])) {
  	$attributes['class'][] = $pane->css['css_class'];
  }

  $output = '<div '.drupal_attributes($attributes).'>';
  $output .= '<div class="content">'.render($content->content).'</div>';
  if (!empty($content->title)) {
    $output .= '<h4>'.$content->title.'</h4>';
  }
  $output .= '</div>';

  return $output;
}