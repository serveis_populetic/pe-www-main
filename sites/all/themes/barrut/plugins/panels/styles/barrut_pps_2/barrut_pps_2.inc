<?php

$plugin = array(
	'title' => t('Barrut: panel with right aligned panes'),
	'render region' => 'barrut_pps_2',
);

function theme_barrut_pps_2($variables) {
  drupal_add_css(drupal_get_path('theme', 'barrut').'/plugins/panels/styles/barrut_pps_2/barrut_pps_2.css');

  $output = '<div class="barrut-pps-2">';
  $output .= implode($variables['panes']);
  $output .= '</div>';  
  return $output;
}