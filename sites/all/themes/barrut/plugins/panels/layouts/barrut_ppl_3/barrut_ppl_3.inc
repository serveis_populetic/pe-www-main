<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 33/67'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_3.png',
  'theme' => 'panel_barrut_ppl_3',
  'css' => 'barrut_ppl_3.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
