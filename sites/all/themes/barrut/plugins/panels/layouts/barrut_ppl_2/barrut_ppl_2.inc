<?php

// Plugin definition
$plugin = array(
  'title' => t('Three column 15/15/70'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_2.png',
  'theme' => 'panel_barrut_ppl_2',
  'css' => 'barrut_ppl_2.css',
  'regions' => array(
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side')
   ),
);
