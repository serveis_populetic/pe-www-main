<?php

// Plugin definition
$plugin = array(
  'title' => t('One row and Two column 70/30'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_6.png',
  'theme' => 'panel_barrut_ppl_6',
  'css' => 'barrut_ppl_6.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
