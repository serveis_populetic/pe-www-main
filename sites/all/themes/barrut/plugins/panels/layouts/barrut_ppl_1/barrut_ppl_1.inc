<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 70/30'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_1.png',
  'theme' => 'panel_barrut_ppl_1',
  'css' => 'barrut_ppl_1.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
