<?php

// Plugin definition
$plugin = array(
  'title' => t('One columns with $class'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_9.png',
  'theme' => 'panel_barrut_ppl_9',
  'css' => 'barrut_ppl_9.css',
  'regions' => array(
		'center' => t('Center side'),
  ),
);
