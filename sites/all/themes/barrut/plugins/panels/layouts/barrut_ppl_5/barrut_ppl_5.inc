<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 40/60'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_5.png',
  'theme' => 'panel_barrut_ppl_5',
  'css' => 'barrut_ppl_5.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
