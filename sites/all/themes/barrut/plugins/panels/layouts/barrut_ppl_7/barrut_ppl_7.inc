<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 30/70 and one row'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_7.png',
  'theme' => 'panel_barrut_ppl_7',
  'css' => 'barrut_ppl_7.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
