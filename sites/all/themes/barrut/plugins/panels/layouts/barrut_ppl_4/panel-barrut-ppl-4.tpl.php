<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-display panel-barrut-ppl-4 clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-panel small left">
    <?php print $content['first']; ?>
  </div>
  <div class="panel-panel big left">
    <?php print $content['second']; ?>
  </div>
  <div class="panel-panel big left">
    <?php print $content['third']; ?>
  </div>
  <div class="panel-panel small right">
    <?php print $content['fourth']; ?>
  </div>
</div>
