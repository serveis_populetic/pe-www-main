<?php

// Plugin definition
$plugin = array(
  'title' => t('Four column 25/25/25/25'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_4.png',
  'theme' => 'panel_barrut_ppl_4',
  'css' => 'barrut_ppl_4.css',
  'regions' => array(
    'first' => t('First'),
    'second' => t('Secodn'),
    'third' => t('Third'),
    'fourth' => t('Fourth'),
  ),
);
