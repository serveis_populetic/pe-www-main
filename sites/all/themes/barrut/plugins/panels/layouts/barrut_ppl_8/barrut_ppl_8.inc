<?php

// Plugin definition
$plugin = array(
  'title' => t('Three columns 35/35/25'),
  'category' => t('Barrut'),
  'icon' => 'barrut_ppl_8.png',
  'theme' => 'panel_barrut_ppl_8',
  'css' => 'barrut_ppl_8.css',
  'regions' => array(
    'left' => t('Left side'),
		'center' => t('Center side'),
    'right' => t('Right side')
  ),
);
